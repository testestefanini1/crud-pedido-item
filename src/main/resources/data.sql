CREATE TABLE item (
  id_produto BIGINT GENERATED BY DEFAULT AS IDENTITY NOT NULL,
   descricao VARCHAR(255),
   quantidade INT,
   valor DOUBLE PRECISION,
   CONSTRAINT pk_item PRIMARY KEY (id_produto)
);

CREATE TABLE pedido (
  id BIGINT GENERATED BY DEFAULT AS IDENTITY NOT NULL,
   client_name VARCHAR(255),
   tipo VARCHAR(255),
   cnpj_ou_cpf VARCHAR(255),
   data_compra date,
   valor_total DOUBLE PRECISION,
   pedido_id BIGINT, -- Coluna para a chave estrangeira
   CONSTRAINT pk_pedido PRIMARY KEY (id)
);

-- Inserir dados na tabela Item
INSERT INTO Item (descricao, quantidade, valor)
VALUES ('Figura de ação Anakin Skywalker', 200, 90.00);
INSERT INTO Item (descricao, quantidade, valor)
VALUES ('Figura de ação Obi-wan', 200, 100.00);
INSERT INTO Item (descricao, quantidade, valor)
VALUES ('Figura de ação Kanan Jarrus', 200, 89.00);
INSERT INTO Item (descricao, quantidade, valor)
VALUES ('Sabre de luz Count Dooku', 200, 150.00);
INSERT INTO Item (descricao, quantidade, valor)
VALUES ('Ghost espaço-nave', 200, 150.00);

