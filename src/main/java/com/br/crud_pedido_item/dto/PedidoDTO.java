package com.br.crud_pedido_item.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PedidoDTO {

    @JsonProperty("idProduto")
    private Long id;
    @JsonProperty("clientName")
    private String clientName;
    @JsonProperty("tipo")
    private String tipo;
    @JsonProperty("cnpjOuCpf")
    private String cnpjOuCpf;
    @JsonProperty("dataCompra")
    private LocalDate dataCompra;
    @JsonProperty("valorTotal")
    private Double valorTotal;
    @JsonProperty("itens")
    private List<ItemDTO> itens;
}
