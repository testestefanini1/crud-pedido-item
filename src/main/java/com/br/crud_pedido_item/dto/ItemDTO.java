package com.br.crud_pedido_item.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ItemDTO {

    @JsonProperty("idProduto")
    private Long idProduto;
    @JsonProperty("descricao")
    private String descricao;
    @JsonProperty("quantidade")
    private Integer quantidade;
    @JsonProperty("valor")
    private Double valor;

}
