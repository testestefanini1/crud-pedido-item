package com.br.crud_pedido_item.repository;

import com.br.crud_pedido_item.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface ItemRepository extends JpaRepository<Item, Long> {

    //metodo para encontrar item por descricao independente se foi solicitado em lowercase ou uppercase
    @Query("SELECT i FROM Item i WHERE LOWER(i.descricao) = LOWER(:descricao)")
    Optional<Item> findByDescricao(@Param("descricao")String descricao);

}
