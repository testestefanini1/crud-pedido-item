package com.br.crud_pedido_item.repository;

import com.br.crud_pedido_item.model.Pedido;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PedidoRepository  extends JpaRepository<Pedido, Long> {

}
