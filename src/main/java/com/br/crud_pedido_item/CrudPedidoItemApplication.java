package com.br.crud_pedido_item;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudPedidoItemApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudPedidoItemApplication.class, args);
	}

}
