package com.br.crud_pedido_item.controller;



import com.br.crud_pedido_item.dto.ItemDTO;
import com.br.crud_pedido_item.dto.PedidoDTO;
import com.br.crud_pedido_item.exception.InvalidInputException;
import com.br.crud_pedido_item.exception.PedidoNotFoundException;
import com.br.crud_pedido_item.mapper.ItemMapper;
import com.br.crud_pedido_item.mapper.PedidoMapper;
import com.br.crud_pedido_item.model.Item;
import com.br.crud_pedido_item.model.Pedido;
import com.br.crud_pedido_item.service.PedidoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/pedidos")
@Tag(name = "PedidoController", description = "Gerenciamento de pedidos")
public class PedidoController {
    @Autowired
    PedidoService pedidoService;
    @Autowired
    PedidoMapper pedidoMapper;
    @Autowired
    ItemMapper itemmapper;



    @Operation(
            summary = "cria um novo pedido",
            description = "cria um novo pedido de acordo com o DTO e insere no banco de dados")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Criação de pedido realizada com sucesso"),
            @ApiResponse(responseCode = "500", description = "Erro interno ao criar pedido"),
            @ApiResponse(responseCode = "400", description = "Necessário verificar se body está correto")
    })
    @PostMapping("/add")
    public ResponseEntity<?> criarItem(@RequestBody PedidoDTO pedidoDTO) {
        Pedido pedido = pedidoMapper.pedidoDTOToPedido(pedidoDTO);
        try{
            Pedido savedPedido = pedidoService.criarPedido(pedido);
            return ResponseEntity.ok(pedidoMapper.pedidoToPedidoDTO(pedido));
        }catch (InvalidInputException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("bad request, por favor, verifique se cpf/cnpj é valido e se campos\n clientName,"
                    +  "tipo, "
                    + "cnpjOuCpf e itens estão preenchidos");
        }catch(Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Operation(
            summary = "Remove um pedido existente",
            description = "Remove um pedido existente por id no banco de dados")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Remoção de pedido realizada com sucesso"),
            @ApiResponse(responseCode = "500", description = "Erro interno ao remover o pedido"),
            @ApiResponse(responseCode = "404", description = "Este pedido já não existe")
    })
    @DeleteMapping("delete/{id}")
    public ResponseEntity<Void> deletarPedido(@PathVariable Long id) {
        if(pedidoService.deletarPedido(id)){
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.notFound().build();
    }

    @Operation(
            summary = "Procura um pedido por ID",
            description = "Procura um pedido existente por id no banco de dados")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Busca realizada com sucesso"),
            @ApiResponse(responseCode = "500", description = "Erro interno ao buscar o pedido"),
            @ApiResponse(responseCode = "404", description = "O pedido não está cadastrado"),
    })
    @GetMapping("buscar/{id}")
    public ResponseEntity<PedidoDTO> buscarPedidoPorId(@PathVariable Long id) {
        try {
            Pedido pedido = pedidoService.buscarPedidoPorId(id);
            return ResponseEntity.ok(pedidoMapper.pedidoToPedidoDTO(pedido));
        }catch (PedidoNotFoundException e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
    @Operation(
            summary = "Procura um item pelo nome/descricao",
            description = "Procura um item existente pelo nome/descricao no banco de dados")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Busca realizada com sucesso"),
    })
    @GetMapping("/listar")
    public ResponseEntity<List<PedidoDTO>> listarTodosPedidos() {

        List<Pedido> pedidos = pedidoService.listarTodosPedidos();
        return ResponseEntity.ok(pedidoMapper.pedidosToPedidoDTOs(pedidos));
    }
    @PostMapping("/{id}/addItem")
    public ResponseEntity<PedidoDTO> adicionarItem(@PathVariable Long id, @RequestBody ItemDTO item) {
        Pedido pedido = pedidoService.buscarPedidoPorId(id);
        pedidoService.adicionarItem(pedido, itemmapper.itemDTOToItem(item));
        return ResponseEntity.ok(pedidoMapper.pedidoToPedidoDTO(pedido));
    }

    @PostMapping("/{id}/additens")
    public ResponseEntity<PedidoDTO> adicionarItens(@PathVariable Long id, @RequestBody List<ItemDTO> itens) {
        Pedido pedido = pedidoService.buscarPedidoPorId(id);
        pedidoService.adicionarItem(pedido, itemmapper.itemDTOsToItems(itens));
        return ResponseEntity.ok(pedidoMapper.pedidoToPedidoDTO(pedido));
    }
    @PutMapping("/alterar/{id}")
    public ResponseEntity<PedidoDTO> modificarPedido(@PathVariable Long id, @RequestBody PedidoDTO pedido) {
        Pedido pedidoAtualizado = pedidoService.modificarPedido(id, pedidoMapper.pedidoDTOToPedido(pedido));
        return ResponseEntity.ok(pedidoMapper.pedidoToPedidoDTO(pedidoAtualizado));
    }



}
