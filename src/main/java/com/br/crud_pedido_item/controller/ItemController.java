package com.br.crud_pedido_item.controller;

import com.br.crud_pedido_item.dto.ItemDTO;
import com.br.crud_pedido_item.exception.InvalidInputException;
import com.br.crud_pedido_item.exception.ItemNotFoundException;
import com.br.crud_pedido_item.mapper.ItemMapper;
import com.br.crud_pedido_item.model.Item;
import com.br.crud_pedido_item.service.ItemService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/itens")
@Tag(name = "ItemController", description = "Gerenciamento de itens")
public class ItemController {
    @Autowired
    private ItemService itemService;
    @Autowired
    ItemMapper itemmapper;


    @Operation(
            summary = "cria um novo item",
            description = "cria um novo item de acordo com o DTO e insere no banco de dados")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Criação de item realizada com sucesso"),
            @ApiResponse(responseCode = "500", description = "Erro interno ao criar item"),
            @ApiResponse(responseCode = "400", description = "Necessário verificar se body está correto")
    })
    @PostMapping("/criar")
    public ResponseEntity<?> criarItem(@RequestBody ItemDTO itemDTO) {
        Item item = itemmapper.itemDTOToItem(itemDTO);
        try{
            Item savedItem = itemService.criarItem(item);
            return ResponseEntity.ok(itemmapper.itemToItemDTO(savedItem));
        }catch (InvalidInputException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Por favor, preencha todos os campos menos o id");
        }catch(Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Operation(
            summary = "Atualiza um item existente",
            description = "Atualiza um item existente por id no banco de dados")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Atualização de item realizada com sucesso"),
            @ApiResponse(responseCode = "500", description = "Erro interno ao atualizar o item")
    })
    @PutMapping("/atualizar/{id}")
    public ResponseEntity<ItemDTO> atualizarItem(@PathVariable Long id, @RequestBody ItemDTO itemDTO) {
        Item item =  itemmapper.itemDTOToItem(itemDTO);
        try{
            Item updatedItem = itemService.atualizarItem(id, item);
            return ResponseEntity.ok(itemmapper.itemToItemDTO(updatedItem));
        }catch(Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Operation(
            summary = "Remove um item existente",
            description = "Remove um item existente por id no banco de dados")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Remoção de item realizada com sucesso"),
            @ApiResponse(responseCode = "500", description = "Erro interno ao remover o item"),
            @ApiResponse(responseCode = "404", description = "Este item já não existe")
    })
    @DeleteMapping("/remover/{id}")
    public ResponseEntity<Void> removerItem(@PathVariable Long id) {
        if (itemService.removerItem(id)) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @Operation(
            summary = "Procura um item por ID",
            description = "Procura um item existente por id no banco de dados")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Busca realizada com sucesso"),
            @ApiResponse(responseCode = "500", description = "Erro interno ao buscar o item"),
            @ApiResponse(responseCode = "404", description = "O item não está cadastrado"),
    })
    @GetMapping("/buscar/id/{id}")
    public ResponseEntity<ItemDTO> buscarItemPorID(@PathVariable Long id) {

        try{
            Item item = itemService.buscarItemPorID(id);
            return ResponseEntity.ok(itemmapper.itemToItemDTO(item));
        }catch(ItemNotFoundException e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

    }

    @Operation(
            summary = "Procura um item pelo nome/descricao",
            description = "Procura um item existente pelo nome/descricao no banco de dados")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Busca realizada com sucesso"),
            @ApiResponse(responseCode = "500", description = "Erro interno ao buscar o item"),
            @ApiResponse(responseCode = "404", description = "O item não está cadastrado"),
    })
    @GetMapping("/buscarbyDescricao")
    public ResponseEntity<ItemDTO> buscarItemPorDescricao(@RequestParam String descricao) {
        try{
            Item item = itemService.buscarItemPorDescricao(descricao);
            return ResponseEntity.ok(itemmapper.itemToItemDTO(item));
        }catch(ItemNotFoundException e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/listar")
    public ResponseEntity<List<ItemDTO>> listarTodosItens() {

        List<Item> itens = itemService.listarTodosItens();

        return ResponseEntity.ok(itemmapper.itemsToItemDTOs(itens));
    }
}
