package com.br.crud_pedido_item.mapper;

import com.br.crud_pedido_item.dto.PedidoDTO;
import com.br.crud_pedido_item.model.Pedido;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface  PedidoMapper {

    PedidoMapper INSTANCE = Mappers.getMapper(PedidoMapper.class);

    PedidoDTO pedidoToPedidoDTO(Pedido pedido);
    List<PedidoDTO> pedidosToPedidoDTOs(List<Pedido> pedidos);
    Pedido pedidoDTOToPedido(PedidoDTO pedidoDTO);
    List<Pedido> pedidoDTOsToPedidos(List<PedidoDTO> pedidoDTOs);


}
