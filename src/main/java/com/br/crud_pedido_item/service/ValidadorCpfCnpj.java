package com.br.crud_pedido_item.service;

public interface ValidadorCpfCnpj {
    boolean isValidCPF(String cpf);
    boolean isValidCNPJ(String cnpj);
}
