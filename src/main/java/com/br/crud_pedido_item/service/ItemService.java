package com.br.crud_pedido_item.service;

import com.br.crud_pedido_item.model.Item;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public interface ItemService {
    Item criarItem(Item item);
    Item atualizarItem(Long id, Item item);
    boolean removerItem(Long id);
    Item buscarItemPorID(Long id);
    Item buscarItemPorDescricao(String descricao);
    List<Item> listarTodosItens();

}
