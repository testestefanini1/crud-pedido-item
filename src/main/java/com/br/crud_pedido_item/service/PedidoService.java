package com.br.crud_pedido_item.service;

import com.br.crud_pedido_item.model.Item;
import com.br.crud_pedido_item.model.Pedido;

import java.util.List;

public interface PedidoService {

    Pedido criarPedido(Pedido pedido);
    Pedido modificarPedido(Long id,Pedido pedido);
    boolean deletarPedido(Long id);
    Pedido buscarPedidoPorId(Long id);
    List<Pedido> listarTodosPedidos();

     void adicionarItem(Pedido pedido, Item item);
    void adicionarItem(Pedido pedido, List<Item> itens);

}
