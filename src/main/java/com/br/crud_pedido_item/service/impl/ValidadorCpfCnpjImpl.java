package com.br.crud_pedido_item.service.impl;

import com.br.crud_pedido_item.service.ValidadorCpfCnpj;
import org.springframework.stereotype.Service;

@Service
public class ValidadorCpfCnpjImpl implements ValidadorCpfCnpj {


    @Override
    public boolean isValidCNPJ(String cnpj) {

        // Remove caracteres não numéricos
        cnpj = cnpj.replaceAll("\\D", "");

        if (cnpj.length() != 14) {
            return false;
        }
        if (cnpj.matches("(\\d)\\1{13}")) {
            return false;
        }

        // Calcula e verifica os dígitos verificadores
        int[] digits = new int[14];
        for (int i = 0; i < 14; i++) {
            digits[i] = cnpj.charAt(i) - '0';
        }
        int sum1 = 0;
        int sum2 = 0;
        int[] weights1 = {5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2};
        int[] weights2 = {6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2};
        for (int i = 0; i < 12; i++) {
            sum1 += digits[i] * weights1[i];
            sum2 += digits[i] * weights2[i];
        }
        int mod1 = sum1 % 11;
        int mod2 = sum2 % 11;
        if (mod1 < 2) {
            if (digits[12] != 0) {
                return false;
            }
        } else {
            if (digits[12] != 11 - mod1) {
                return false;
            }
        }
        if (mod2 < 2) {
            if (digits[13] != 0) {
                return false;
            }
        } else {
            if (digits[13] != 11 - mod2) {
                return false;
            }
        }

        return true;
    }

    @Override
    public boolean isValidCPF(String cpf) {
        // Remove caracteres não numéricos
        cpf = cpf.replaceAll("\\D", "");

        if (cpf.length() != 11) {
            return false;
        }

        // Verifica se todos os dígitos são iguais
        if (cpf.matches("(\\d)\\1{10}")) {
            return false;
        }

        // Calcula e verifica os dígitos verificadores
        int[] digits = new int[11];
        for (int i = 0; i < 11; i++) {
            digits[i] = cpf.charAt(i) - '0';
        }
        int sum1 = 0;
        int sum2 = 0;
        for (int i = 0; i < 9; i++) {
            sum1 += digits[i] * (10 - i);
            sum2 += digits[i] * (11 - i);
        }
        int mod1 = sum1 % 11;
        int mod2 = sum2 % 11;
        if (mod1 < 2) {
            if (digits[9] != 0) {
                return false;
            }
        } else {
            if (digits[9] != 11 - mod1) {
                return false;
            }
        }
        if (mod2 < 2) {
            if (digits[10] != 0) {
                return false;
            }
        }

        return true;
    }
}
