package com.br.crud_pedido_item.service.impl;

import com.br.crud_pedido_item.exception.InvalidInputException;
import com.br.crud_pedido_item.exception.ItemNotFoundException;
import com.br.crud_pedido_item.model.Item;
import com.br.crud_pedido_item.repository.ItemRepository;
import com.br.crud_pedido_item.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ItemServiceImpl implements ItemService {

    @Autowired
    private ItemRepository itemRepository;

    public Item criarItem(Item item) {
        validarCamposObrigatorios(item);
        return itemRepository.save(item);
    }
    @Override
    public Item atualizarItem(Long id, Item item) {

        Item existingItem = itemRepository.findById(id)
                .orElseThrow(() -> new ItemNotFoundException("Item não encontrado com ID: " + id));

        // Atualiza se não for nulo
        if (item.getDescricao() != null) {
            existingItem.setDescricao(item.getDescricao());
        }
        if (item.getQuantidade() != null) {
            existingItem.setQuantidade(item.getQuantidade());
        }
        if (item.getValor() != null) {
            existingItem.setValor(item.getValor());
        }

        return itemRepository.save(existingItem);
    }


    @Override
    public boolean removerItem(Long id) {
        return itemRepository.findById(id).map(item -> {
            itemRepository.delete(item);
            return true;
        }).orElseThrow(() -> new ItemNotFoundException("Item não encontrado com ID: " + id));
    }

    @Override
    public Item buscarItemPorID(Long id) {
        return itemRepository.findById(id).orElseThrow(() -> new ItemNotFoundException("Item não encontrado com ID: " + id));
    }


    @Override
    public Item buscarItemPorDescricao(String descricao) {
        return itemRepository.findByDescricao(descricao).orElseThrow( () -> new ItemNotFoundException("Item não encontrado com ID: " + descricao));
    }

    @Override
    public List<Item> listarTodosItens() {
        return itemRepository.findAll();
    }

    private void validarCamposObrigatorios(Item item) {
        if (item.getDescricao() == null || item.getQuantidade() == null || item.getValor() == null) {
            throw new InvalidInputException("Os campos descricao, quantidade e valor são obrigatórios.");
        }
    }
}

