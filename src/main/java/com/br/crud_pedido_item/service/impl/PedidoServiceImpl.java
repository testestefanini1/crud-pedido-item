package com.br.crud_pedido_item.service.impl;

import com.br.crud_pedido_item.exception.InvalidInputException;
import com.br.crud_pedido_item.exception.PedidoNotFoundException;
import com.br.crud_pedido_item.model.Item;
import com.br.crud_pedido_item.model.Pedido;
import com.br.crud_pedido_item.repository.PedidoRepository;
import com.br.crud_pedido_item.service.ItemService;
import com.br.crud_pedido_item.service.PedidoService;
import com.br.crud_pedido_item.service.ValidadorCpfCnpj;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class PedidoServiceImpl implements PedidoService {
    @Autowired
    ValidadorCpfCnpj validadorCpfCnpj;
    @Autowired
    PedidoRepository pedidoRepository;
    @Autowired
    ItemService itemService;

    @Override
    public Pedido criarPedido(Pedido pedido) {
        validarPedido(pedido);
        pedido.setDataCompra(LocalDate.now());
        pedido.setValorTotal(calcularValorTotal(pedido));
        return pedidoRepository.save(pedido);
    }

    @Override
    public boolean deletarPedido(Long id) {
        return pedidoRepository.findById(id).map(pedido -> {
            pedidoRepository.delete(pedido);
            return true;
                }).orElseThrow(() -> new PedidoNotFoundException("Pedido não encontrado com ID: " + id));

        //Pedido pedido = pedidoRepository.findById(id)
        //        .orElseThrow(() -> new PedidoNotFoundException("Pedido não encontrado com ID: " + id));
       // pedidoRepository.delete(pedido);
        //return true;
    }

    @Override
    public Pedido buscarPedidoPorId(Long id) {
        return pedidoRepository.findById(id)
                .orElseThrow(() -> new PedidoNotFoundException("Pedido não encontrado com ID: " + id));
    }

    @Override
    public List<Pedido> listarTodosPedidos() {
        return pedidoRepository.findAll();
    }

    @Override
    public void adicionarItem(Pedido pedido, Item item) {

        Item itemNoEstoque = itemService.buscarItemPorID(item.getIdProduto());
        if (itemNoEstoque.getQuantidade() < item.getQuantidade()) {
            throw new InvalidInputException("Quantidade insuficiente no estoque para o item: " + item.getDescricao());
        }
        // Atualiza a quantidade do item no estoque
        itemNoEstoque.setQuantidade(itemNoEstoque.getQuantidade() - item.getQuantidade());
        itemService.atualizarItem(itemNoEstoque.getIdProduto(), itemNoEstoque);

        // Adiciona o item ao pedido
        pedido.getItens().add(item);
        pedido.setValorTotal(calcularValorTotal(pedido));
        pedidoRepository.save(pedido);
    }

    // Overload do método adicionarItem
    public void adicionarItem(Pedido pedido, List<Item> itens) {
        for (Item item : itens) {
            adicionarItem(pedido, item);
        }
    }

    @Override
    public Pedido modificarPedido(Long id, Pedido pedido) {
        Pedido pedidoExistente = pedidoRepository.findById(id)
                .orElseThrow(() -> new PedidoNotFoundException("Pedido não encontrado com ID: " + id));

        // Atualiza apenas os campos não nulos
        if (pedido.getClientName() != null) {
            pedidoExistente.setClientName(pedido.getClientName());
        }
        if (pedido.getTipo() != null) {
            pedidoExistente.setTipo(pedido.getTipo());
        }
        if (pedido.getCnpjOuCpf() != null) {
            pedidoExistente.setCnpjOuCpf(pedido.getCnpjOuCpf());
        }
        if (pedido.getItens() != null) {
            pedidoExistente.setItens(pedido.getItens());
            // Recalcular o valor total quando salvar
            //pedidoExistente.setValorTotal(calcularValorTotal(pedidoExistente));
        }
        if (pedido.getDataCompra() != null) {
            pedidoExistente.setDataCompra(pedido.getDataCompra());
        }

        // Se os itens não foram atualizados, mantém o valor total existente
        if (pedido.getItens() == null) {
            pedidoExistente.setValorTotal(calcularValorTotal(pedidoExistente));
        }

        return pedidoRepository.save(pedidoExistente);
    }
    private double calcularImpostoCPF(double valorTotal){
        return valorTotal * 0.052;
    }
    private double calcularImpostoCNPJ(double valorTotal){
        return 1 + (valorTotal * 0.032);
    }

    private double calcularValorTotal(Pedido pedido) {
        double valorTotal = pedido.getItens().stream()
                .mapToDouble(item -> item.getQuantidade() * item.getValor())
                .sum();

        if (pedido.getTipo().equalsIgnoreCase("CPF")) {
            valorTotal += calcularImpostoCPF(valorTotal);
        } else if (pedido.getTipo().equalsIgnoreCase("CNPJ")) {
            valorTotal += calcularImpostoCNPJ(valorTotal);
        }

        return valorTotal;
    }

    private void validarPedido(Pedido pedido) {
        if (pedido.getClientName() == null || pedido.getTipo() == null || pedido.getCnpjOuCpf() == null || pedido.getItens() == null) {
            throw new InvalidInputException("Todos os campos do pedido são obrigatórios.");
        }

        if (pedido.getTipo().equalsIgnoreCase("CPF") && !validadorCpfCnpj.isValidCPF(pedido.getCnpjOuCpf())) {
            throw new InvalidInputException("CPF inválido.");
        }

        if (pedido.getTipo().equalsIgnoreCase("CNPJ") && !validadorCpfCnpj.isValidCNPJ(pedido.getCnpjOuCpf())) {
            throw new InvalidInputException("CNPJ inválido.");
        }
    }

}
