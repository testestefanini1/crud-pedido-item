package com.br.crud_pedido_item.model;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "ITEM")
@Data
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idProduto;
    private String descricao;
    private Integer quantidade;
    private Double valor;

}
