package com.br.crud_pedido_item.model;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Entity
@Data
@Table(name = "PEDIDO")
public class Pedido {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String clientName;
    private String tipo;
    private String cnpjOuCpf;
    private LocalDate dataCompra;
    private Double valorTotal;

    @OneToMany
    private List<Item> itens;
}
